/* Common code for sasl-client, sasl-server, sasx-client, sasx-server.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <arpa2/com_err.h>
#include <arpa2/except.h>
#include <arpa2/socket.h>

#include <arpa2/quick-der.h>
#include <arpa2/quick-sasl.h>
#include <arpa2/quick-dermem.h>



/* Send output that can be parsed on the other end.
 * The output format is one line
 *    prompt> he xb yt es
 * unless no token is provided, then it is
 *    prompt> 
 */
void prompted_hex_send (void *master, FILE *stream, char *prompt, dercursor data) {
	//
	// Have an output buffer and ensure it is large enough
	char output_buffer [2000];
	assert (                                      data.derlen + 1             < sizeof (output_buffer));
	assert (strlen (prompt) + strlen ("> ") + 3 * data.derlen + strlen ("\n") < sizeof (output_buffer));
	//
	// Either send NULL when we have no data, or otherwise the bytes
	if (data.derptr == NULL) {
		sprintf (output_buffer, "%s> NULL\n", prompt);
	} else {
		int i;
		for (i=0; i < data.derlen; i++) {
			char ch = isprint (data.derptr [i]) ? data.derptr [i] : '.';
			output_buffer [i] = ch;
		}
		output_buffer [i  ] = '\0';
		printf ("Sending %zd bytes prompted %s: %s\n", data.derlen, prompt, output_buffer);
		int endpos = sprintf (output_buffer, "%s>", prompt);
		for (i=0; i < data.derlen; i++) {
			endpos += sprintf (output_buffer + endpos, "%s%02x",
						((i % 8 == 0) && (i != 0)) ? "  " : " ",
						data.derptr [i]);
		}
		output_buffer [endpos++] = '\n';
		output_buffer [endpos  ] = '\0';
	}
	//
	// Output the buffer
	if (data.derptr != NULL) {
	}
	fputs (output_buffer, stream);
	fflush (stream);
}


/* Determine the hex value of a single character.  Assert checks syntax.
 */
uint8_t hexvalue (char c) {
	assert (isxdigit (c));
	if (isdigit (c)) {
		return (c - '0');
	} else {
		return (10 + tolower (c) - 'a');
	}
}


/* Receive input from the other end.
 * TODO: A format for no data.
 */
void prompted_hex_recv (void *master, FILE *stream, const char **ok_prompts, const char **out_prompt, dercursor *out_data) {
	//
	// Have a large input buffer and read one line
	char input_buffer [2020];
prompted_lineread:
	assert (fgets (input_buffer, sizeof (input_buffer) - 1, stream) != NULL);
	char *split = strchr (input_buffer, '>');
	if (split == NULL) {
		goto prompted_lineread;
	}
	*split = '\0';
	//
	// Only pass prompts known to us -- TODO: could ignore whitespace
	char *instr = input_buffer;
	while (isspace (*instr)) {
		instr++;
	}
	const char **try_prompt = ok_prompts;
	while (*try_prompt) {
		if (strcmp (*try_prompt, input_buffer) == 0) {
			break;
		}
		try_prompt++;
	}
	if (*try_prompt == NULL) {
		printf ("\n*\n* Skipping unrecognised prompt: %s\n*\n\n", instr);
		goto prompted_lineread;
	}
	*out_prompt = *try_prompt;
	//
	// If we got the string "NULL", which is not valid hex, return DER NULL
	dercursor binbuf = {
		.derptr = NULL,
		.derlen = 0
	};
	instr = split + 1;
	while ((*instr) && (isspace (*instr))) {
		instr++;
	}
	if (strncmp (instr, "NULL", 4) == 0) {
		printf ("Received NULL prompted %s\n", *out_prompt);
		*out_data = binbuf;
		return;
	}
	//
	// Prepare a DER MEM buffer large enough for the HEX data
	assert (dermem_buffer_open (master, strlen (instr) / 2, &binbuf));
	//
	// Parse the HEX bytes, simply use assert() for syntax checks
	size_t numbytes = 0;
	while (*instr) {
		// Skip spaces, including line endings
		while (isspace (*instr)) {
			instr++;
		}
		// Did we reach the end of the line?
		if (*instr == '\0') {
			break;
		}
		// Read two HEX digits
		uint8_t hex = 0;
		hex += (hexvalue (*instr++) << 4);
		hex += (hexvalue (*instr++)     );
		binbuf.derptr [numbytes++] = hex;
	}
	//
	// Close the buffer
	dermem_buffer_close (master, numbytes, &binbuf);
	printf ("Received %zd == %zd bytes prompted %s: %02x %02x %02x %02x...\n", binbuf.derlen, numbytes, *out_prompt, binbuf.derptr[0], binbuf.derptr[1], binbuf.derptr[2], binbuf.derptr[3]);
	*out_data = binbuf;
}


/* Callback, setting a pointed-to state to the provided state.
 */
void cb_statechange (void *state, QuickSASL _qs, qsaslt_state newstate) {
	qsaslt_state *qstate = state;
	*qstate = newstate;
}
